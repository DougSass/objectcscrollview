//
//  ViewController.m
//  ObjectCScrollView
//
//  Created by Douglas Sass on 1/13/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//
//  Created an app that is a scrollview and has 9 different views
//  with different background colors and will be able to scroll through all nine sections
//
//              -----------------------------
//
//              |   1    |   2     |    3   |
//
//              -----------------------------
//
//              |   4    |   5     |    6   |
//
//              -----------------------------
//
//              |   7    |    8    |    9   |
//
//              -----------------------------


#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
        //Sets the size of the viewable area for the application
        UIScrollView* sv = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [self.view addSubview:sv];
    

        //Row 1 containing views 1, 2, and 3
    
        //Create View 1 with the background color yellow
        UIView* view1yellow = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        view1yellow.backgroundColor = [UIColor yellowColor];
    
        //Adds subview  view1yellow to the ScrollView sv
        [sv addSubview:view1yellow];
    
    
        //Create View 2 with the background color purple
        UIView* view2purple = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)];
        view2purple.backgroundColor = [UIColor purpleColor];

        //Adds subview  view2purple to the ScrollView sv
        [sv addSubview:view2purple];
    
    
        //Create View 3 with the background color green
        UIView* view3green = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width * 2, 0, self.view.frame.size.width, self.view.frame.size.height)];
        view3green.backgroundColor = [UIColor greenColor];
    
        //Adds subview  view3green to the ScrollView sv
        [sv addSubview:view3green];
    
    
        //Row 2 containing views 4, 5, and 6
    
        //Create View 4 with the background color blue
        UIView* view4blue = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        view4blue.backgroundColor = [UIColor blueColor];

        //Adds subview  view4blue to the ScrollView sv
        [sv addSubview:view4blue];
    
    
        //Create View 5 with the background color red
        UIView* view5red = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        view5red.backgroundColor = [UIColor redColor];
    
        //Adds subview  view5red to the ScrollView sv
        [sv addSubview:view5red];
    

        //Create View 6 with the background color orange
        UIView* view6orange = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width * 2, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
        view6orange.backgroundColor = [UIColor orangeColor];
    
        //Adds subview  view6orange to the ScrollView sv
        [sv addSubview:view6orange];
    
    
        //Row 3 containing views 7, 8, and 9
    
        //Create View 7 with the background color cyan
        UIView* view7cyan = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height * 2, self.view.frame.size.width, self.view.frame.size.height)];
        view7cyan.backgroundColor = [UIColor cyanColor];
    
        //Adds subview  view7cyan to the ScrollView sv
        [sv addSubview:view7cyan];
    
    
        //Create View 8 with the background color gray
        UIView* view8gray = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height * 2, self.view.frame.size.width, self.view.frame.size.height)];
        view8gray.backgroundColor = [UIColor grayColor];
    
        //Adds subview  view8gray to the ScrollView sv
        [sv addSubview:view8gray];
    
    
        //Create View 9 with the background color brown
        UIView* view9brown = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width * 2, self.view.frame.size.height * 2, self.view.frame.size.width, self.view.frame.size.height)];
        view9brown.backgroundColor = [UIColor brownColor];
    
        //Adds subview  view9brown to the ScrollView sv
        [sv addSubview:view9brown];
    
    
        //Creates the size of the content that the views can fill
        [sv setContentSize:CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height * 3)];
        [sv setPagingEnabled:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
